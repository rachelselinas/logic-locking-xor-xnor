#TO COMPILE CODE
#(i) Using make file
make 
#(ii) Using perl script 
perl compile.pl

#Actual compile commands executed - for reference
#mkdir obj 
#g++-4.8 -c -fPIC -W -Wall -Wextra -Wreturn-type  -std=c++98 -Wno-deprecated -Wno-unused-local-typedefs -Wno-ignored-qualifiers -I /home/polaris/rsraj20/simple_xor_impl/Limbo-master/limbo/parsers/verilog/bison/ -I /home/polaris/rsraj20/simple_xor_impl/Limbo-master -I./ main.cpp -o obj/main.o
#g++-4.8 -O3 -fopenmp -fmax-errors=1 -fPIC -W -Wall -Wextra -Wreturn-type -m64  -std=c++98 -Wno-deprecated -Wno-unused-local-typedefs -Wno-ignored-qualifiers  -o main obj/main.o -L /home/polaris/rsraj20/simple_xor_impl/Limbo-master/lib -lverilogparser
#g++-4.8 -O3 -fopenmp -fmax-errors=1 -fPIC -W -Wall -Wextra -Wreturn-type -m64  -std=c++98 -Wno-deprecated -Wno-unused-local-typedefs -Wno-ignored-qualifiers  main.cpp -MM -MT obj/main.o >obj/main.d -I /home/polaris/rsraj20/simple_xor_impl/Limbo-master/limbo/parsers/verilog/bison -I /home/polaris/rsraj20/simple_xor_impl/Limbo-master -I./

#TO RUN CODE
./main <input_file>.v <output_file>.v

Example: 
./main benchmarks/simple.v out.v
