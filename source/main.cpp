//Implement XOR logic locking

#include<iostream>
#include<fstream>
#include<random>
#include<algorithm>
#include<string>
#include "limbo/parsers/verilog/bison/VerilogDriver.h"
#include "netlist_elements.h"

using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::cin;

int main (int argc, char **argv) 
{
	if (argc > 2) xor_implementation(argv[1], argv[2]);
	else 
		std::cout << "Provide input and output verilog files!" << std::endl;
	return 0;
}
