#!/usr/bin/perl
use strict;
use warnings;

#clean operation
print("Removing files from previous compilation... \n");
system("rm -rf obj");
system("rm -rf main");

# Compiling main.cpp file with limbo verilog parser library
print("Compiling main.cpp...\n");
system("mkdir obj");
system("g++-4.8 -c -fPIC -W -Wall -Wextra -Wreturn-type  -std=c++11 -Wno-deprecated -Wno-unused-local-typedefs -Wno-ignored-qualifiers -I /home/polaris/rsraj20/simple_xor_impl/Limbo-master/limbo/parsers/verilog/bison/ -I /home/polaris/rsraj20/simple_xor_impl/Limbo-master -I./ main.cpp -o obj/main.o");
system("g++-4.8 -O3 -fopenmp -fPIC -W -Wall -Wextra -Wreturn-type -m64  -std=c++11 -Wno-deprecated -Wno-unused-local-typedefs -Wno-ignored-qualifiers  -o main obj/main.o -L /home/polaris/rsraj20/simple_xor_impl/Limbo-master/lib -lverilogparser");
system("g++-4.8 -O3 -fopenmp -fPIC -W -Wall -Wextra -Wreturn-type -m64  -std=c++11 -Wno-deprecated -Wno-unused-local-typedefs -Wno-ignored-qualifiers  main.cpp -MM -MT obj/main.o >obj/main.d -I /home/polaris/rsraj20/simple_xor_impl/Limbo-master/limbo/parsers/verilog/bison -I /home/polaris/rsraj20/simple_xor_impl/Limbo-master -I./");

