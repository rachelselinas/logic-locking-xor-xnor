module divider_subcircuit ( A, B, CI, DIFF, CO );
  input [32:0] A;
  input [32:0] B;
  output [32:0] DIFF;
  input CI;
  output CO;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n37, n39, n40, n41, n42, n43, n45, n47, n48,
         n49, n50, n51, n53, n55, n56, n57, n58, n59, n61, n63, n64, n65, n66,
         n67, n69, n71, n72, n73, n74, n75, n76, n77, n78, n79, n81, n83, n84,
         n87, n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100,
         n101, n102, n103, n104, n106, n107, n108, n109, n110, n111, n112,
         n113, n114, n115, n116, n117, n118, n119, n120, n121, n122, n123,
         n124, n125, n126, n127, n128, n129, n130, n131, n132, n133, n134,
         n135, n137, n138, n139, n140, n141, n142, n143, n144, n145, n146,
         n147, n148, n149, n150, n151, n152, n153, n154, n156, n158, n160,
         n162, n164, n166, n168, n169, n170, n171, n172, n173, n174, n175,
         n176, n177, n178, n179, n180, n181, n182, n183, n184, n185, n186,
         n187, n188, n189, n190, n191, n192, n193, n194, n195, n196, n197,
         n198, n199, n200, n201, n202, n203, n204, n205, n206, n207, n208,
         n209, n210, n319, n320, n321, n322, n323, n324, n325, n326, n327,
         n328, n329;

  FA_X1 U2 ( .A(n179), .B(A[31]), .CI(n27), .CO(n26), .S(DIFF[31]) );
  FA_X1 U4 ( .A(n181), .B(A[29]), .CI(n29), .CO(n28), .S(DIFF[29]) );
  FA_X1 U5 ( .A(n182), .B(A[28]), .CI(n30), .CO(n29), .S(DIFF[28]) );
  FA_X1 U6 ( .A(n183), .B(A[27]), .CI(n31), .CO(n30), .S(DIFF[27]) );
  FA_X1 U7 ( .A(n184), .B(A[26]), .CI(n32), .CO(n31), .S(DIFF[26]) );
  NOR2_X1 U241 ( .A1(n197), .A2(A[13]), .ZN(n89) );
  NOR2_X1 U242 ( .A1(n208), .A2(A[2]), .ZN(n147) );
  NOR2_X1 U243 ( .A1(n205), .A2(A[5]), .ZN(n133) );
  NOR2_X1 U244 ( .A1(n203), .A2(A[7]), .ZN(n125) );
  NOR2_X1 U245 ( .A1(n204), .A2(A[6]), .ZN(n128) );
  NOR2_X1 U246 ( .A1(n210), .A2(A[0]), .ZN(n153) );
  AOI21_X1 U247 ( .B1(n48), .B2(n322), .A(n45), .ZN(n43) );
  AOI21_X1 U248 ( .B1(n56), .B2(n323), .A(n53), .ZN(n51) );
  AOI21_X1 U249 ( .B1(n100), .B2(n113), .A(n101), .ZN(n99) );
  NOR2_X2 U253 ( .A1(n199), .A2(A[11]), .ZN(n102) );
  NAND2_X1 U256 ( .A1(n198), .A2(A[12]), .ZN(n95) );
  NAND2_X1 U257 ( .A1(n208), .A2(A[2]), .ZN(n148) );
  INV_X1 U259 ( .A(n97), .ZN(n96) );
  NAND2_X1 U260 ( .A1(n112), .A2(n100), .ZN(n98) );
  INV_X1 U261 ( .A(n120), .ZN(n119) );
  AOI21_X1 U262 ( .B1(n140), .B2(n131), .A(n132), .ZN(n130) );
  OAI21_X1 U263 ( .B1(n119), .B2(n110), .A(n111), .ZN(n109) );
  INV_X1 U264 ( .A(n112), .ZN(n110) );
  OAI21_X1 U265 ( .B1(n119), .B2(n98), .A(n99), .ZN(n97) );
  INV_X1 U266 ( .A(n141), .ZN(n140) );
  INV_X1 U270 ( .A(n150), .ZN(n149) );
  OAI21_X1 U271 ( .B1(n67), .B2(n65), .A(n66), .ZN(n64) );
  OAI21_X1 U272 ( .B1(n59), .B2(n57), .A(n58), .ZN(n56) );
  OAI21_X1 U273 ( .B1(n75), .B2(n73), .A(n74), .ZN(n72) );
  OAI21_X1 U274 ( .B1(n51), .B2(n49), .A(n50), .ZN(n48) );
  OAI21_X1 U275 ( .B1(n114), .B2(n118), .A(n115), .ZN(n113) );
  OAI21_X1 U276 ( .B1(n102), .B2(n108), .A(n103), .ZN(n101) );
  OAI21_X1 U277 ( .B1(n133), .B2(n139), .A(n134), .ZN(n132) );
  OAI21_X1 U278 ( .B1(n43), .B2(n41), .A(n42), .ZN(n40) );
  INV_X1 U279 ( .A(n39), .ZN(n37) );
  INV_X1 U280 ( .A(n55), .ZN(n53) );
  INV_X1 U281 ( .A(n47), .ZN(n45) );
  NOR2_X1 U282 ( .A1(n138), .A2(n133), .ZN(n131) );
  OAI21_X1 U283 ( .B1(n151), .B2(n153), .A(n152), .ZN(n150) );
  NOR2_X1 U284 ( .A1(n107), .A2(n102), .ZN(n100) );
  OAI21_X1 U285 ( .B1(n89), .B2(n95), .A(n90), .ZN(n88) );
  OAI21_X1 U286 ( .B1(n141), .B2(n121), .A(n122), .ZN(n120) );
  NAND2_X1 U287 ( .A1(n131), .A2(n123), .ZN(n121) );
  AOI21_X1 U288 ( .B1(n123), .B2(n132), .A(n124), .ZN(n122) );
  NOR2_X1 U289 ( .A1(n128), .A2(n125), .ZN(n123) );
  AOI21_X1 U290 ( .B1(n88), .B2(n319), .A(n81), .ZN(n79) );
  INV_X1 U291 ( .A(n83), .ZN(n81) );
  NOR2_X1 U292 ( .A1(n98), .A2(n78), .ZN(n76) );
  OAI21_X1 U293 ( .B1(n99), .B2(n78), .A(n79), .ZN(n77) );
  NAND2_X1 U294 ( .A1(n87), .A2(n319), .ZN(n78) );
  AOI21_X1 U295 ( .B1(n72), .B2(n321), .A(n69), .ZN(n67) );
  INV_X1 U296 ( .A(n71), .ZN(n69) );
  INV_X1 U297 ( .A(n63), .ZN(n61) );
  NOR2_X1 U298 ( .A1(n94), .A2(n89), .ZN(n87) );
  NOR2_X1 U299 ( .A1(n117), .A2(n114), .ZN(n112) );
  AOI21_X1 U300 ( .B1(n142), .B2(n150), .A(n143), .ZN(n141) );
  NOR2_X1 U301 ( .A1(n147), .A2(n144), .ZN(n142) );
  OAI21_X1 U302 ( .B1(n144), .B2(n148), .A(n145), .ZN(n143) );
  OAI21_X1 U303 ( .B1(n125), .B2(n129), .A(n126), .ZN(n124) );
  AOI21_X1 U304 ( .B1(n97), .B2(n92), .A(n93), .ZN(n91) );
  INV_X1 U305 ( .A(n95), .ZN(n93) );
  AOI21_X1 U306 ( .B1(n109), .B2(n169), .A(n106), .ZN(n104) );
  INV_X1 U307 ( .A(n108), .ZN(n106) );
  AOI21_X1 U308 ( .B1(n140), .B2(n175), .A(n137), .ZN(n135) );
  INV_X1 U309 ( .A(n139), .ZN(n137) );
  INV_X1 U310 ( .A(n94), .ZN(n92) );
  INV_X1 U311 ( .A(n107), .ZN(n169) );
  INV_X1 U312 ( .A(n138), .ZN(n175) );
  OAI21_X1 U313 ( .B1(n119), .B2(n117), .A(n118), .ZN(n116) );
  OAI21_X1 U314 ( .B1(n130), .B2(n128), .A(n129), .ZN(n127) );
  OAI21_X1 U315 ( .B1(n149), .B2(n147), .A(n148), .ZN(n146) );
  INV_X1 U316 ( .A(n117), .ZN(n171) );
  INV_X1 U317 ( .A(n128), .ZN(n173) );
  INV_X1 U318 ( .A(n147), .ZN(n177) );
  INV_X1 U319 ( .A(n89), .ZN(n166) );
  INV_X1 U320 ( .A(n125), .ZN(n172) );
  INV_X1 U321 ( .A(n133), .ZN(n174) );
  INV_X1 U322 ( .A(n144), .ZN(n176) );
  INV_X1 U323 ( .A(n114), .ZN(n170) );
  INV_X1 U324 ( .A(n151), .ZN(n178) );
  INV_X1 U325 ( .A(n41), .ZN(n156) );
  INV_X1 U326 ( .A(n49), .ZN(n158) );
  INV_X1 U327 ( .A(n57), .ZN(n160) );
  INV_X1 U328 ( .A(n65), .ZN(n162) );
  INV_X1 U329 ( .A(n73), .ZN(n164) );
  INV_X1 U330 ( .A(n33), .ZN(n154) );
  INV_X1 U331 ( .A(n102), .ZN(n168) );
  NOR2_X1 U332 ( .A1(n202), .A2(A[8]), .ZN(n117) );
  NOR2_X2 U333 ( .A1(n207), .A2(A[3]), .ZN(n144) );
  XOR2_X1 U334 ( .A(n135), .B(n21), .Z(DIFF[5]) );
  NAND2_X1 U335 ( .A1(n174), .A2(n134), .ZN(n21) );
  XNOR2_X1 U336 ( .A(n140), .B(n22), .ZN(DIFF[4]) );
  NAND2_X1 U337 ( .A1(n175), .A2(n139), .ZN(n22) );
  XNOR2_X1 U338 ( .A(n146), .B(n23), .ZN(DIFF[3]) );
  NAND2_X1 U339 ( .A1(n176), .A2(n145), .ZN(n23) );
  XOR2_X1 U340 ( .A(n149), .B(n24), .Z(DIFF[2]) );
  NAND2_X1 U341 ( .A1(n177), .A2(n148), .ZN(n24) );
  XOR2_X1 U342 ( .A(n25), .B(n153), .Z(DIFF[1]) );
  NAND2_X1 U343 ( .A1(n178), .A2(n152), .ZN(n25) );
  NOR2_X2 U344 ( .A1(n201), .A2(A[9]), .ZN(n114) );
  OR2_X1 U345 ( .A1(n196), .A2(A[14]), .ZN(n319) );
  NOR2_X1 U346 ( .A1(n200), .A2(A[10]), .ZN(n107) );
  NOR2_X1 U347 ( .A1(n198), .A2(A[12]), .ZN(n94) );
  NOR2_X1 U348 ( .A1(n206), .A2(A[4]), .ZN(n138) );
  NOR2_X1 U349 ( .A1(n209), .A2(A[1]), .ZN(n151) );
  INV_X1 U350 ( .A(B[29]), .ZN(n181) );
  NAND2_X1 U351 ( .A1(n154), .A2(n34), .ZN(n1) );
  NAND2_X1 U352 ( .A1(n324), .A2(n39), .ZN(n2) );
  XOR2_X1 U353 ( .A(n43), .B(n3), .Z(DIFF[23]) );
  NAND2_X1 U354 ( .A1(n156), .A2(n42), .ZN(n3) );
  NAND2_X1 U355 ( .A1(n322), .A2(n47), .ZN(n4) );
  XOR2_X1 U356 ( .A(n51), .B(n5), .Z(DIFF[21]) );
  NAND2_X1 U357 ( .A1(n158), .A2(n50), .ZN(n5) );
  XNOR2_X1 U358 ( .A(n56), .B(n6), .ZN(DIFF[20]) );
  NAND2_X1 U359 ( .A1(n323), .A2(n55), .ZN(n6) );
  NAND2_X1 U360 ( .A1(n160), .A2(n58), .ZN(n7) );
  XNOR2_X1 U361 ( .A(n64), .B(n8), .ZN(DIFF[18]) );
  NAND2_X1 U362 ( .A1(n320), .A2(n63), .ZN(n8) );
  NAND2_X1 U363 ( .A1(n162), .A2(n66), .ZN(n9) );
  NAND2_X1 U364 ( .A1(n321), .A2(n71), .ZN(n10) );
  NAND2_X1 U365 ( .A1(n164), .A2(n74), .ZN(n11) );
  XNOR2_X1 U366 ( .A(n84), .B(n12), .ZN(DIFF[14]) );
  NAND2_X1 U367 ( .A1(n319), .A2(n83), .ZN(n12) );
  XOR2_X1 U368 ( .A(n91), .B(n13), .Z(DIFF[13]) );
  NAND2_X1 U369 ( .A1(n166), .A2(n90), .ZN(n13) );
  XOR2_X1 U370 ( .A(n96), .B(n14), .Z(DIFF[12]) );
  NAND2_X1 U371 ( .A1(n92), .A2(n95), .ZN(n14) );
  XOR2_X1 U372 ( .A(n104), .B(n15), .Z(DIFF[11]) );
  NAND2_X1 U373 ( .A1(n168), .A2(n103), .ZN(n15) );
  XNOR2_X1 U374 ( .A(n109), .B(n16), .ZN(DIFF[10]) );
  NAND2_X1 U375 ( .A1(n169), .A2(n108), .ZN(n16) );
  XNOR2_X1 U376 ( .A(n116), .B(n17), .ZN(DIFF[9]) );
  NAND2_X1 U377 ( .A1(n170), .A2(n115), .ZN(n17) );
  XOR2_X1 U378 ( .A(n119), .B(n18), .Z(DIFF[8]) );
  NAND2_X1 U379 ( .A1(n171), .A2(n118), .ZN(n18) );
  XNOR2_X1 U380 ( .A(n127), .B(n19), .ZN(DIFF[7]) );
  NAND2_X1 U381 ( .A1(n172), .A2(n126), .ZN(n19) );
  XOR2_X1 U382 ( .A(n130), .B(n20), .Z(DIFF[6]) );
  NAND2_X1 U383 ( .A1(n173), .A2(n129), .ZN(n20) );
  XNOR2_X1 U384 ( .A(n210), .B(A[0]), .ZN(DIFF[0]) );
  INV_X1 U385 ( .A(B[13]), .ZN(n197) );
  INV_X1 U386 ( .A(B[12]), .ZN(n198) );
  INV_X1 U387 ( .A(B[6]), .ZN(n204) );
  INV_X1 U388 ( .A(B[10]), .ZN(n200) );
  INV_X1 U389 ( .A(B[2]), .ZN(n208) );
  INV_X1 U390 ( .A(B[8]), .ZN(n202) );
  INV_X1 U391 ( .A(B[5]), .ZN(n205) );
  INV_X1 U392 ( .A(B[7]), .ZN(n203) );
  INV_X1 U393 ( .A(B[1]), .ZN(n209) );
  INV_X1 U394 ( .A(B[9]), .ZN(n201) );
  INV_X1 U395 ( .A(B[3]), .ZN(n207) );
  INV_X1 U396 ( .A(B[4]), .ZN(n206) );
  INV_X1 U397 ( .A(B[18]), .ZN(n192) );
  INV_X1 U398 ( .A(B[17]), .ZN(n193) );
  INV_X1 U399 ( .A(B[16]), .ZN(n194) );
  INV_X1 U400 ( .A(B[15]), .ZN(n195) );
  INV_X1 U401 ( .A(B[14]), .ZN(n196) );
  NOR2_X1 U402 ( .A1(n193), .A2(A[17]), .ZN(n65) );
  NOR2_X1 U403 ( .A1(n195), .A2(A[15]), .ZN(n73) );
  NAND2_X1 U404 ( .A1(n204), .A2(A[6]), .ZN(n129) );
  INV_X1 U405 ( .A(B[0]), .ZN(n210) );
  NAND2_X1 U406 ( .A1(n206), .A2(A[4]), .ZN(n139) );
  NAND2_X1 U407 ( .A1(n202), .A2(A[8]), .ZN(n118) );
  INV_X1 U408 ( .A(B[26]), .ZN(n184) );
  INV_X1 U409 ( .A(B[27]), .ZN(n183) );
  INV_X1 U410 ( .A(B[28]), .ZN(n182) );
  NAND2_X1 U411 ( .A1(n200), .A2(A[10]), .ZN(n108) );
  INV_X1 U412 ( .A(B[31]), .ZN(n179) );
  NAND2_X1 U413 ( .A1(n207), .A2(A[3]), .ZN(n145) );
  NAND2_X1 U414 ( .A1(n197), .A2(A[13]), .ZN(n90) );
  NAND2_X1 U415 ( .A1(n203), .A2(A[7]), .ZN(n126) );
  NAND2_X1 U416 ( .A1(n205), .A2(A[5]), .ZN(n134) );
  NAND2_X1 U417 ( .A1(n209), .A2(A[1]), .ZN(n152) );
  OR2_X1 U418 ( .A1(n192), .A2(A[18]), .ZN(n320) );
  OR2_X1 U419 ( .A1(n194), .A2(A[16]), .ZN(n321) );
  NAND2_X1 U420 ( .A1(n192), .A2(A[18]), .ZN(n63) );
  NAND2_X1 U421 ( .A1(n194), .A2(A[16]), .ZN(n71) );
  NAND2_X1 U422 ( .A1(n196), .A2(A[14]), .ZN(n83) );
  NAND2_X1 U423 ( .A1(n201), .A2(A[9]), .ZN(n115) );
  NAND2_X1 U424 ( .A1(n193), .A2(A[17]), .ZN(n66) );
  NAND2_X1 U425 ( .A1(n195), .A2(A[15]), .ZN(n74) );
  INV_X1 U426 ( .A(B[25]), .ZN(n185) );
  INV_X1 U427 ( .A(B[24]), .ZN(n186) );
  INV_X1 U428 ( .A(B[23]), .ZN(n187) );
  INV_X1 U429 ( .A(B[22]), .ZN(n188) );
  INV_X1 U430 ( .A(B[21]), .ZN(n189) );
  INV_X1 U431 ( .A(B[20]), .ZN(n190) );
  INV_X1 U432 ( .A(B[19]), .ZN(n191) );
  OR2_X1 U433 ( .A1(n188), .A2(A[22]), .ZN(n322) );
  OR2_X1 U434 ( .A1(n190), .A2(A[20]), .ZN(n323) );
  NOR2_X1 U435 ( .A1(n187), .A2(A[23]), .ZN(n41) );
  NOR2_X1 U436 ( .A1(n189), .A2(A[21]), .ZN(n49) );
  NOR2_X1 U437 ( .A1(n191), .A2(A[19]), .ZN(n57) );
  NOR2_X1 U438 ( .A1(n185), .A2(A[25]), .ZN(n33) );
  OR2_X1 U439 ( .A1(n186), .A2(A[24]), .ZN(n324) );
  NAND2_X1 U440 ( .A1(n186), .A2(A[24]), .ZN(n39) );
  NAND2_X1 U441 ( .A1(n188), .A2(A[22]), .ZN(n47) );
  NAND2_X1 U442 ( .A1(n190), .A2(A[20]), .ZN(n55) );
  NAND2_X1 U443 ( .A1(n187), .A2(A[23]), .ZN(n42) );
  NAND2_X1 U444 ( .A1(n189), .A2(A[21]), .ZN(n50) );
  NAND2_X1 U445 ( .A1(n191), .A2(A[19]), .ZN(n58) );
  NAND2_X1 U446 ( .A1(n185), .A2(A[25]), .ZN(n34) );
  XOR2_X1 U447 ( .A(n180), .B(A[30]), .Z(n325) );
  XOR2_X1 U448 ( .A(n28), .B(n325), .Z(DIFF[30]) );
  NAND2_X1 U449 ( .A1(n28), .A2(n180), .ZN(n326) );
  NAND2_X1 U450 ( .A1(n28), .A2(A[30]), .ZN(n327) );
  NAND2_X1 U451 ( .A1(n180), .A2(A[30]), .ZN(n328) );
  NAND3_X1 U452 ( .A1(n326), .A2(n328), .A3(n327), .ZN(n27) );
  INV_X1 U453 ( .A(B[30]), .ZN(n180) );
  XNOR2_X1 U454 ( .A(n40), .B(n2), .ZN(DIFF[24]) );
  XNOR2_X1 U455 ( .A(n72), .B(n10), .ZN(DIFF[16]) );
  XOR2_X1 U456 ( .A(n59), .B(n7), .Z(DIFF[19]) );
  XOR2_X1 U457 ( .A(n67), .B(n9), .Z(DIFF[17]) );
  XOR2_X1 U458 ( .A(n75), .B(n11), .Z(DIFF[15]) );
  INV_X1 U459 ( .A(n113), .ZN(n111) );
  NAND2_X1 U460 ( .A1(n199), .A2(A[11]), .ZN(n103) );
  INV_X1 U461 ( .A(n26), .ZN(DIFF[32]) );
  XNOR2_X1 U462 ( .A(n48), .B(n4), .ZN(DIFF[22]) );
  XOR2_X1 U463 ( .A(n35), .B(n1), .Z(DIFF[25]) );
  OAI21_X1 U464 ( .B1(n35), .B2(n33), .A(n34), .ZN(n32) );
  INV_X1 U465 ( .A(B[11]), .ZN(n199) );
  AOI21_X2 U251 ( .B1(n40), .B2(n324), .A(n37), .ZN(n35) );
  AOI21_X2 U252 ( .B1(n64), .B2(n320), .A(n61), .ZN(n59) );
  AOI21_X2 U250 ( .B1(n120), .B2(n76), .A(n77), .ZN(n75) );
  INV_X1 U254 ( .A(n329), .ZN(n84) );
  AOI21_X1 U255 ( .B1(n97), .B2(n87), .A(n88), .ZN(n329) );
endmodule
