#include<iostream>
#include<fstream>
#include <sstream>
#include<random>
#include<algorithm>
#include<string>
#include "limbo/parsers/verilog/bison/VerilogDriver.h"

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::vector;

template<typename T>
std::string toString(const T& value)
{
    std::ostringstream oss;
    oss << value;
    return oss.str();
}

class Pin	//Primary input/output pins as well as cell pins
{
	public:
		std::string pin__name;
		int pin_dir; //1- input; 2-output; 0 - if unknown
		int r_low;
		int r_high;
};

class Net 
{
	public:
		std::string net__name;
		std::vector<Pin> net_conns;
		int r_low;
		int r_high;
		bool operator == (const Net& netn) {
			if (net__name == netn.net__name) {
				int sum(0), sum1(0);
				if (r_low >=0) sum += r_low;
				if (r_high >=0) sum += r_high;
				if (netn.r_low >=0) sum1 += netn.r_low;
				if (netn.r_high >=0) sum1 += netn.r_high;
				if (sum == sum1) return true;
				else return false;
			} else return false;
		}
};

class Cell 
{
	public:
		std::string cell_type, cell_name;
		std::vector<Pin> pin_conns;
		std::vector<Net> net_conns;
		//std::unordered_map<Pin, Net> pin2net_conns;
};

Net find_out_net_of_cell (Cell &cell_n) 
{
	int pin_size = cell_n.pin_conns.size();
	for (int i = 0; i < pin_size; ++i) if ((cell_n.pin_conns[i].pin__name == "Z") || (cell_n.pin_conns[i].pin__name == "ZN")|| (cell_n.pin_conns[i].pin__name == "o")|| (cell_n.pin_conns[i].pin__name == "q")|| (cell_n.pin_conns[i].pin__name == "Q") || (cell_n.pin_conns[i].pin__name == "CO") || (cell_n.pin_conns[i].pin__name == "S")) return cell_n.net_conns[i];
	std::cout << "Output net of cell: " << cell_n.cell_name << " not found!!" << std::endl;
	abort();
}

void modify_out_net_cell (Cell &cell_n, Net &net_n)
{
	int pin_size = cell_n.pin_conns.size();
	for (int i=0; i<pin_size; ++i) if ((cell_n.pin_conns[i].pin__name == "Z") || (cell_n.pin_conns[i].pin__name == "ZN")|| (cell_n.pin_conns[i].pin__name == "o")|| (cell_n.pin_conns[i].pin__name == "q")|| (cell_n.pin_conns[i].pin__name == "Q") || (cell_n.pin_conns[i].pin__name == "CO") || (cell_n.pin_conns[i].pin__name == "S")) {
		cell_n.net_conns[i] = net_n;
		break;
	}
}

Cell create_new_cell (std::string &cell_name, std::string &cell_type)
{
	Cell newcell;
	replace(cell_name.begin(), cell_name.end(), '/', '_');
	newcell.cell_name = cell_name;
	newcell.cell_type = cell_type;
	return newcell;
}
Pin create_new_pin (std::string pin_name, int pin_dir, int rlow, int rhigh)
{
	Pin newpin;
	newpin.pin__name = pin_name;
	replace(pin_name.begin(), pin_name.end(), '/', '_');
	newpin.pin_dir = pin_dir;
	newpin.r_low = rlow;
	newpin.r_high = rhigh;
	return newpin;
}
Net create_new_net (std::string &net_name, int rlow, int rhigh)
{
	Net newnet;
	replace(net_name.begin(), net_name.end(), '/', '_');
	newnet.net__name = net_name;
	newnet.r_low = rlow;
	newnet.r_high = rhigh;
	return newnet;
}

class VerilogDataBase : public VerilogParser::VerilogDataBase
{
	public:
		vector<Cell> all_cells;
		vector<Pin> all_ports;
		vector<Net> all_nets;
		//assign statements for nets
		vector<Net> eq_lhs, eq_rhs;
        /// @brief constructor 
		VerilogDataBase()
		{
			//std::cout << "VerilogDataBase::" << __func__ << std::endl;
		}
		//////////////////// required callbacks from abstract VerilogParser::VerilogDataBase ///////////////////
        /// @brief read an instance. 
        /// 
        /// NOR2_X1 u2 ( .a(n1), .b(n3), .o(n2) );
        /// 
        /// @param macro_name standard cell type or module name 
        /// @param inst_name instance name 
        /// @param vNetPin array of pairs of net and pin 
        virtual void verilog_instance_cbk(std::string const& macro_name, std::string const& inst_name, std::vector<VerilogParser::NetPin> const& vNetPin)
        {
			//std::cout << __func__ << " => " << macro_name << ", " << inst_name << ", ";
		Cell newcell;
		newcell.cell_type = macro_name;
		newcell.cell_name = inst_name;
            for (std::vector<VerilogParser::NetPin>::const_iterator it = vNetPin.begin(); it != vNetPin.end(); ++it)
	    {
                //std::cout << it->pin << "(" << it->net << ")" << "[" << it->range.low << ":" << it->range.high << "] ";
		Pin clpin;
		clpin.pin__name = it->pin;
		clpin.pin_dir = 0;
		clpin.r_low = -1;
		clpin.r_high = -1;
		Net clnet;
		clnet.net__name = it->net;
		if (it->range.low == it->range.high) {
			clnet.r_low = -1;
			clnet.r_high = -1;
		} else {
			clnet.r_low = it->range.low;
			clnet.r_high = it->range.high;
		}
		clnet.net_conns.push_back(clpin);
		newcell.pin_conns.push_back(clpin);
		newcell.net_conns.push_back(clnet);
		bool found(false);
		int total_ports = all_ports.size();
		int total_nets = all_nets.size();
		for (int i = 0; i < total_ports; ++i) if (all_ports[i].pin__name == clnet.net__name) found = true;
		for (int i = 0; i < total_nets; ++i) if (all_nets[i].net__name == clnet.net__name) found = true;
		if(found == false)all_nets.push_back(clnet);
	    }
            //std::cout << std::endl;
	    all_cells.push_back(newcell);
        }
        /// @brief read an net declaration 
        /// 
        /// wire aaa[1];
        /// 
        /// @param net_name net name 
        /// @param range net range, negative infinity if either low or high value of the range is not defined 
        virtual void verilog_net_declare_cbk(std::string const& net_name, VerilogParser::Range const& range)
		{
			Net newnet;
			newnet.net__name = net_name;
			if (range.low == range.high) {
				newnet.r_low = -1;
				newnet.r_high = -1;
			} else {
				newnet.r_low = range.low;
				newnet.r_high = range.high;
			}
			//std::cout << __func__ << " => " << net_name << " (" << range.low << ", " << range.high << ")" << std::endl;
			bool found(false);
			int total_ports = all_ports.size();
			int total_nets = all_nets.size();
		for (int i = 0; i < total_ports; ++i) if (all_ports[i].pin__name == newnet.net__name) found = true;
		for (int i = 0; i < total_nets; ++i) if (all_nets[i].net__name == newnet.net__name) found = true;
			if(found == false)all_nets.push_back(newnet);
		}
        /// @brief read an pin declaration 
        /// 
        /// input inp2;
        /// 
        /// @param pin_name pin name 
        /// @param type type of pin, refer to @ref VerilogParser::PinType
        /// @param range pin range, negative infinity if either low or high value of the range is not defined 
        virtual void verilog_pin_declare_cbk(std::string const& pin_name, unsigned type, VerilogParser::Range const& range)
	{
		Pin port_pin;
		port_pin.pin_dir = type;
		port_pin.pin__name = pin_name;
		if(range.low == range.high) {
			port_pin.r_low = -1;
			port_pin.r_high = -1;
		} else {
			port_pin.r_low = range.low;
			port_pin.r_high = range.high;
		}
		all_ports.push_back(port_pin);
		//std::cout << __func__ << " => " << pin_name << " " << type << " (" << range.low << ", " << range.high << ")" << std::endl;
	}
        /// @brief read an assignment 
        /// 
        /// assign exu_mmu_early_va_e[0] = exu_mmu_early_va_e[0];
        /// 
        /// @param target_name name of left hand side 
        /// @param target_range range of left hand side, negative infinity if either low or high value of the range is not defined 
        /// @param source_name name of right hand side 
        /// @param source_range range of right hand side, negative infinity if either low or high value of the range is not defined 
        virtual void verilog_assignment_cbk(std::string const& target_name, VerilogParser::Range const& target_range, std::string const& source_name, VerilogParser::Range const& source_range)
	{
		//std::cout << __func__ << " => " << target_name << " (" << target_range.low << ", " << target_range.high << ")" << " = " 
		//<< source_name << " (" << source_range.low << ", " << source_range.high << ")" << std::endl;
		Net net1, net2;
		net1.net__name = target_name;
		if (target_range.low == target_range.high) {
			net1.r_low = -1;
			net1.r_high = -1;
		} else {
			net1.r_low = target_range.low;
			net1.r_high = target_range.high;
		}
		net2.net__name = source_name;
		if (source_range.low == source_range.high) {
			net2.r_low = -1;
			net2.r_high = -1;
		} else {
			net2.r_low = source_range.low;
			net2.r_high = source_range.high;
		}
		eq_lhs.push_back(net1);
		bool found(false);
		int total_ports = all_ports.size();
		int total_nets = all_nets.size();
		for (int i = 0; i < total_ports; ++i) if (all_ports[i].pin__name == net1.net__name) found = true;
		for (int i = 0; i < total_nets; ++i) if (all_nets[i].net__name == net1.net__name) found = true;
		if (found == false)all_nets.push_back(net1);
		eq_rhs.push_back(net2);
		found = false;
		int total_net = all_nets.size();
		for (int i = 0; i < total_ports; ++i) if (all_ports[i].pin__name == net2.net__name) found = true;
		for (int i = 0; i < total_net; ++i) if (all_nets[i].net__name == net2.net__name) found = true;
		if (found == false)all_nets.push_back(net2);
	}
};

bool is_port (std::string &net_name, VerilogDataBase &db)
{
	bool found(false);
	int ports_count = db.all_ports.size();
	for (int i=0; i < ports_count; ++i) if (db.all_ports[i].pin__name == net_name) return true;
	return found;
}

void display_ports_cells (VerilogDataBase &db)
{
	//Displaying ports/cells in netlist
	cout << "//Ports: ";
	int ports_count = db.all_ports.size();
	cout << ports_count << endl;
	for (int i = 0 ; i < ports_count ; ++i) {
		//Direction
		if (db.all_ports[i].pin_dir == 1) cout << "input ";
		else cout << "output ";
		//Bus
		if ((db.all_ports[i].r_low < 0) && (db.all_ports[i].r_high < 0)) // cout << "Not bus"; 
		; else if ((db.all_ports[i].r_low >= 0) && (db.all_ports[i].r_high >=0)) cout << "[" << db.all_ports[i].r_low << ":" << db.all_ports[i].r_high << "] " ;
		else if ((db.all_ports[i].r_low >= 0) && (db.all_ports[i].r_high < 0)) cout << "[" << db.all_ports[i].r_low << "] "; 
		else cout << "[" << db.all_ports[i].r_high << "] "; 
		//port
		cout << db.all_ports[i].pin__name ;
		cout << ";" << endl;
	}
	cout << endl;
	cout << "//Wires: ";
	int wires_count = db.all_nets.size();
	cout << wires_count << endl;
	cout << "wire ";
	for (int i=0; i < wires_count; ++i) if (is_port(db.all_nets[i].net__name, db) == false){
	       cout << db.all_nets[i].net__name;
		if ((db.all_nets[i].r_low < 0) && (db.all_nets[i].r_high < 0)) // cout << "Not bus"; 
		; else if ((db.all_nets[i].r_low >= 0) && (db.all_nets[i].r_high >=0)) cout << "[" << db.all_nets[i].r_low << ":" << db.all_nets[i].r_high << "]" ;
		else if ((db.all_nets[i].r_low >= 0) && (db.all_nets[i].r_high < 0)) cout << "[" << db.all_nets[i].r_low << "]"; 
		else cout << "[" << db.all_nets[i].r_high << "]"; 
		if (i < wires_count-1) cout <<", ";
	}
	cout << ";" << endl;
	cout << endl;
	cout << "// Total cells: ";
	int cell_count = db.all_cells.size();
	cout << cell_count << endl;
	for (int j = 0; j < cell_count ; ++j)
	{
		cout <<db.all_cells[j].cell_type << " " << db.all_cells[j].cell_name << " (";
		Cell dcl = db.all_cells[j];
		int pins_size = dcl.pin_conns.size();
		for (int cl = 0; cl < pins_size; ++cl) {
			//pin
			cout << "." << dcl.pin_conns[cl].pin__name;
			//pin bus
			if ((dcl.pin_conns[cl].r_low < 0) && (dcl.pin_conns[cl].r_high < 0)) //cout << "No bus";
		 	; else if ((dcl.pin_conns[cl].r_low >= 0) && (dcl.pin_conns[cl].r_high >=0)) cout << "[" << dcl.pin_conns[cl].r_low << ":" << dcl.pin_conns[cl].r_high << "]";
			else if ((dcl.pin_conns[cl].r_low >= 0) && (dcl.pin_conns[cl].r_high <0)) cout << "[" << dcl.pin_conns[cl].r_low << "]";
			else cout << "[" << dcl.pin_conns[cl].r_high << "]";
			//corresponding net
			cout << " (" << dcl.net_conns[cl].net__name;
			if ((dcl.net_conns[cl].r_low < 0) && (dcl.net_conns[cl].r_high < 0)) //cout << "No bus";
		 	; else if ((dcl.net_conns[cl].r_low >= 0) && (dcl.net_conns[cl].r_high >=0)) cout << "[" << dcl.net_conns[cl].r_low << ":" << dcl.net_conns[cl].r_high << "]";
			else if ((dcl.net_conns[cl].r_low >= 0) && (dcl.net_conns[cl].r_high <0)) cout << "[" << dcl.net_conns[cl].r_low << "]";
			else cout << "[" << dcl.net_conns[cl].r_high << "]";
			if (cl < pins_size-1)cout << " ), "; 
			else cout << ")";
		}
		cout << ");" << endl;
	}
	cout << endl;
	//assigns
	if ((db.eq_lhs.size() >0) && (db.eq_rhs.size() > 0)) {
	cout << "//Assign statements " << endl;
		int total_assigns = db.eq_lhs.size();
		for (int i = 0; i < total_assigns; ++i) {
			cout << "assign " << db.eq_lhs[i].net__name;
			if ((db.eq_lhs[i].r_low < 0) && (db.eq_lhs[i].r_high < 0)) //cout << "No bus";
			; else if ((db.eq_lhs[i].r_low >= 0) && (db.eq_lhs[i].r_high >=0)) cout << "[" << db.eq_lhs[i].r_low << ":" << db.eq_lhs[i].r_high << "]";
			else if ((db.eq_lhs[i].r_low >= 0) && (db.eq_lhs[i].r_high <0)) cout << "[" << db.eq_lhs[i].r_low << "]";
			else cout << "[" << db.eq_lhs[i].r_high << "]";

			cout << " = " << db.eq_rhs[i].net__name;
			if ((db.eq_rhs[i].r_low < 0) && (db.eq_rhs[i].r_high < 0)) //cout << "No bus";
			; else if ((db.eq_rhs[i].r_low >= 0) && (db.eq_rhs[i].r_high >=0)) cout << "[" << db.eq_rhs[i].r_low << ":" << db.eq_rhs[i].r_high << "]";
			else if ((db.eq_rhs[i].r_low >= 0) && (db.eq_rhs[i].r_high <0)) cout << "[" << db.eq_rhs[i].r_low << "]";
			else cout << "[" << db.eq_rhs[i].r_high << "]";
			cout << ";" << endl;
		}
	}
}

void out_file(string const& filein, string const& fileout, VerilogDataBase &db)
{
	std::ofstream fout;
	fout.open(fileout);
	std::ifstream fin;
	fin.open(filein);

	//Reading from input file - to get module name which is not available in parser
	bool found(false);
	while (found == false) {
		std::string in_line;
		std::getline(fin, in_line);
		fout << in_line << endl;
		//Checking for ; after module definition
		std::size_t search = in_line.find(";");
		if(search != std::string::npos) found = true;
		else found = false;
	}

	fout << endl;
	//Port definitions
	int ports_count = db.all_ports.size();
	for (int i = 0 ; i < ports_count ; ++i) {
		//Direction
		if (db.all_ports[i].pin_dir == 1) fout << "  input ";
		else fout << "  output ";
		//Bus
		if ((db.all_ports[i].r_low < 0) && (db.all_ports[i].r_high < 0)) // fout << "Not bus"; 
			; else if ((db.all_ports[i].r_low >= 0) && (db.all_ports[i].r_high >=0)) fout << "[" << db.all_ports[i].r_low << ":" << db.all_ports[i].r_high << "] " ;
		else if ((db.all_ports[i].r_low >= 0) && (db.all_ports[i].r_high < 0)) fout << "[" << db.all_ports[i].r_low << "] "; 
		else fout << "[" << db.all_ports[i].r_high << "] "; 
		//port
		fout << db.all_ports[i].pin__name ;
		fout << ";" << endl;
	}
	fout << endl;
	//Wire definitions
	int wires_count = db.all_nets.size();
	fout << "  wire ";
	for (int i=0; i < wires_count; ++i) if(is_port(db.all_nets[i].net__name, db) == false) {
		fout << db.all_nets[i].net__name;
		if ((db.all_nets[i].r_low < 0) && (db.all_nets[i].r_high < 0)) // fout << "Not bus"; 
			; else if ((db.all_nets[i].r_low >= 0) && (db.all_nets[i].r_high >=0)) fout << "[" << db.all_nets[i].r_low << ":" << db.all_nets[i].r_high << "]" ;
		else if ((db.all_nets[i].r_low >= 0) && (db.all_nets[i].r_high < 0)) fout << "[" << db.all_nets[i].r_low << "]"; 
		else fout << "[" << db.all_nets[i].r_high << "]"; 
		if (i < wires_count-1) fout <<", ";
	}
	fout << ";" << endl;
	fout << endl;
	//Cell definitions
	int cell_count = db.all_cells.size();
	for (int j = 0; j < cell_count ; ++j)
	{
		fout <<"  " << db.all_cells[j].cell_type << " " << db.all_cells[j].cell_name << " ( ";
		Cell dcl = db.all_cells[j];
		int pins_size = dcl.pin_conns.size();
		for (int cl = 0; cl < pins_size; ++cl) {
			//pin
			fout << "." << dcl.pin_conns[cl].pin__name;
			//pin bus
			if ((dcl.pin_conns[cl].r_low < 0) && (dcl.pin_conns[cl].r_high < 0)) //fout << "No bus";
			; else if ((dcl.pin_conns[cl].r_low >= 0) && (dcl.pin_conns[cl].r_high >=0)) fout << "[" << dcl.pin_conns[cl].r_low << ":" << dcl.pin_conns[cl].r_high << "]";
			else if ((dcl.pin_conns[cl].r_low >= 0) && (dcl.pin_conns[cl].r_high <0)) fout << "[" << dcl.pin_conns[cl].r_low << "]";
			else fout << "[" << dcl.pin_conns[cl].r_high << "]";
			//corresponding net
			fout << "(" << dcl.net_conns[cl].net__name;
			if ((dcl.net_conns[cl].r_low < 0) && (dcl.net_conns[cl].r_high < 0)) //fout << "No bus";
			; else if ((dcl.net_conns[cl].r_low >= 0) && (dcl.net_conns[cl].r_high >=0)) fout << "[" << dcl.net_conns[cl].r_low << ":" << dcl.net_conns[cl].r_high << "]";
			else if ((dcl.net_conns[cl].r_low >= 0) && (dcl.net_conns[cl].r_high <0)) fout << "[" << dcl.net_conns[cl].r_low << "]";
			else fout << "[" << dcl.net_conns[cl].r_high << "]";
			if (cl < pins_size-1)fout << "), "; 
			else fout << ") ";
		}
		fout << ");" << endl;
	}
	fout << endl;
	//Assigns if any
	if ((db.eq_lhs.size() >0) && (db.eq_rhs.size() > 0)) {
		int total_assigns = db.eq_lhs.size();
		for (int i = 0; i < total_assigns; ++i) {
			fout << "  assign " << db.eq_lhs[i].net__name;
			if ((db.eq_lhs[i].r_low < 0) && (db.eq_lhs[i].r_high < 0)) //fout << "No bus";
			; else if ((db.eq_lhs[i].r_low >= 0) && (db.eq_lhs[i].r_high >=0)) fout << "[" << db.eq_lhs[i].r_low << ":" << db.eq_lhs[i].r_high << "]";
			else if ((db.eq_lhs[i].r_low >= 0) && (db.eq_lhs[i].r_high <0)) fout << "[" << db.eq_lhs[i].r_low << "]";
			else fout << "[" << db.eq_lhs[i].r_high << "]";

			fout << " = " << db.eq_rhs[i].net__name;
			if ((db.eq_rhs[i].r_low < 0) && (db.eq_rhs[i].r_high < 0)) //fout << "No bus";
			; else if ((db.eq_rhs[i].r_low >= 0) && (db.eq_rhs[i].r_high >=0)) fout << "[" << db.eq_rhs[i].r_low << ":" << db.eq_rhs[i].r_high << "]";
			else if ((db.eq_rhs[i].r_low >= 0) && (db.eq_rhs[i].r_high <0)) fout << "[" << db.eq_rhs[i].r_low << "]";
			else fout << "[" << db.eq_rhs[i].r_high << "]";
			fout << ";" << endl;
		}
	}
	fout << endl;

	fout << "endmodule" << endl;
	fout.close();
}

void xor_implementation(string const& filename, string const& fileout)
{
	//parsing netlist using limbo library
	VerilogDataBase db;
	VerilogParser::read(db, filename);

	int cell_count = db.all_cells.size();
	std::vector<int> clcnt_rand;
	for (int j = 0; j < cell_count ; ++j) clcnt_rand.push_back(j);

	//non-repetitive random number generation
	std::random_shuffle(clcnt_rand.begin(), clcnt_rand.end());

	//Display ports and cells
	display_ports_cells(db);

	//Number of key bits to be added - limit max to number of cells
	std::cout << std::endl;
	std::cout << " Enter number of key inputs to be added: (max " << cell_count << ")" << endl;
	int key_inputs;
	std::cin >> key_inputs;
	if (key_inputs > cell_count) abort();

	std::cout << "Cells to be modified: " << endl;

	for (int i=0; i < key_inputs; ++i) {
		int val = clcnt_rand[i];
		std::cout << val << endl;
		if (val%2) { //odd : Key value is 1; adding XNOR 
			//new input port
			std::string keyo_pin = "key_" + toString(val) + "_";
			Pin keyo = create_new_pin (keyo_pin, 1, -1, -1);
			db.all_ports.push_back(keyo);

			//key to xnor
			std::string key_nnet = keyo.pin__name;
			Net key_n = create_new_net (key_nnet, -1, -1);
			key_n.net_conns.push_back(keyo);

			//output of cell considered
			std::string ogate_xnornet = db.all_cells[val].cell_name + "_out2xnor";
			Net ogate_xnor = create_new_net (ogate_xnornet, -1, -1);
			db.all_nets.push_back(ogate_xnor);

			//creating new XNOR cell to be inserted
			std::string ncell_name = "xnor_" + db.all_cells[val].cell_name + "_out";
			std::string ncell_type = "XNOR2_X1";
			Cell ocell = create_new_cell(ncell_name, ncell_type);
			Pin oxnor_A = create_new_pin ("A", 1, -1, -1);
			ocell.pin_conns.push_back(oxnor_A);
			ocell.net_conns.push_back(ogate_xnor);
			Pin oxnor_B = create_new_pin ("B", 1, -1, -1);
			ocell.pin_conns.push_back(oxnor_B);
			ocell.net_conns.push_back(key_n);

			Pin oxnor_ZN = create_new_pin ("ZN", 2, -1, -1);
			ocell.pin_conns.push_back(oxnor_ZN);
			ocell.net_conns.push_back(find_out_net_of_cell(db.all_cells[val]));
			modify_out_net_cell(db.all_cells[val], ogate_xnor);
			db.all_cells.push_back(ocell);

		} else { //even : Key value is 0; adding XOR
			//new input port
			std::string keypin = "key_" + toString(val) + "_";
			Pin key = create_new_pin (keypin, 1, -1, -1);
			db.all_ports.push_back(key);

			//key to xor
			std::string key_nnet = key.pin__name;
			Net key_n = create_new_net (key_nnet, -1, -1);
			key_n.net_conns.push_back(key);

			//output of cell considered
			std::string ogate_xornet = db.all_cells[val].cell_name + "_out2xor";
			Net ogate_xor = create_new_net (ogate_xornet, -1, -1);
			db.all_nets.push_back(ogate_xor);

			//creating new XOR cell to be inserted
			std::string ncell_name = "xor_" + db.all_cells[val].cell_name + "_out";
			std::string ncell_type = "XOR2_X1"; 
			Cell ncell = create_new_cell(ncell_name, ncell_type);
			Pin nxor_A = create_new_pin ("A", 1, -1, -1);
			ncell.pin_conns.push_back(nxor_A);
			ncell.net_conns.push_back(ogate_xor);
			Pin nxor_B = create_new_pin ("B", 1, -1, -1);
			ncell.pin_conns.push_back(nxor_B);
			ncell.net_conns.push_back(key_n);
			Pin nxor_Z = create_new_pin ("Z", 2, -1, -1);
			ncell.pin_conns.push_back(nxor_Z);
			ncell.net_conns.push_back(find_out_net_of_cell(db.all_cells[val])); 

			modify_out_net_cell(db.all_cells[val],ogate_xor); 
			db.all_cells.push_back(ncell);
		}
	}

	//Display ports and cells
	display_ports_cells(db);

	//Dump output verilog file
	out_file(filename, fileout, db);
}

